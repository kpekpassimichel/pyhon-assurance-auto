AC1=1
AC2=0.9
AC3=0.75
AC4=0.5
AC5=0.45
AC6=0.65
AC7=0.7


def ageCondducteur():
    a = int(input("Age du conducteur : "))

    if (a>=18 & a<=20):
        return AC1
    elif(a>=21 & a<=24):
        return AC2
    elif(a>=25 & a<=29):
        return AC3
    elif(a>=30 & a<=44):
        return AC4
    elif(a>=45 & a<=64):
        return AC5
    elif(a>=65 & a<=74):
        return AC6
    elif(a>=75):
        return AC7
